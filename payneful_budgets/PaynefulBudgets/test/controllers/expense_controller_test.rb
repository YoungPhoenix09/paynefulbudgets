require 'test_helper'

class ExpenseControllerTest < ActionController::TestCase
  test "should get get_expense_details" do
    get :get_expense_details
    assert_response :success
  end

  test "should get add_expense" do
    get :add_expense
    assert_response :success
  end

  test "should get remove_expense" do
    get :remove_expense
    assert_response :success
  end

end
