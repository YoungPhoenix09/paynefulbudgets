require 'test_helper'

class BudgetsControllerTest < ActionController::TestCase
  test "should get create_budget" do
    get :create_budget
    assert_response :success
  end

  test "should get edit_budget" do
    get :edit_budget
    assert_response :success
  end

  test "should get remove_budget" do
    get :remove_budget
    assert_response :success
  end

  test "should get show_budgets" do
    get :show_budgets
    assert_response :success
  end

end
