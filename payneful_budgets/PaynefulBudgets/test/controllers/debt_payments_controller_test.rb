require 'test_helper'

class DebtPaymentsControllerTest < ActionController::TestCase
  test "should get add_payment" do
    get :add_payment
    assert_response :success
  end

  test "should get remove_payment" do
    get :remove_payment
    assert_response :success
  end

  test "should get get_payment_details" do
    get :get_payment_details
    assert_response :success
  end

end
