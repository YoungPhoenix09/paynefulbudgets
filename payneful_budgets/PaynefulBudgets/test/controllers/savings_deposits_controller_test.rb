require 'test_helper'

class SavingsDepositsControllerTest < ActionController::TestCase
  test "should get add_deposit" do
    get :add_deposit
    assert_response :success
  end

  test "should get remove_deposit" do
    get :remove_deposit
    assert_response :success
  end

  test "should get get_deposit_details" do
    get :get_deposit_details
    assert_response :success
  end

end
