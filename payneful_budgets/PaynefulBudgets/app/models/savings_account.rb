class SavingsAccount < ActiveRecord::Base
  belongs_to :budget
  has_many :savings_deposits
  
  def self.create_from_params(form_hash)
    if form_hash.blank? || form_hash.class.to_s != "ActiveSupport::HashWithIndifferentAccess" then
      return nil
    end
    
    item = SavingsAccount.new
    item.name = savingsItem[:savingsName]
    item.category = savingsItem[:savingsCategory]
    item.amount_deposited = savingsItem[:savingsAmount].sub(/\$/, "").to_d
    item.current_amount_deposited = savingsItem[:savingsAmount].sub(/\$/, "").to_d
    
    #Add category for use in later budgets
    category = OtherCategory.where(user_id: user.id, name: item.category, category_type: "savings")
    if category.first == nil then
      category = OtherCategory.new(name: item.category, category_type: "savings")
      user.other_categories << category
      user.save
    end
    
    return item
  end
end
