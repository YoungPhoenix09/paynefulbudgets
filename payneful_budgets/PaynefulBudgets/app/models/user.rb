class User < ActiveRecord::Base
  has_many :accounts
  has_many :budgets, through: :accounts
  has_many :other_categories
  
  has_secure_password
end
