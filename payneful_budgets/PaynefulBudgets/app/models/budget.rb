class Budget < ActiveRecord::Base
  has_many :accounts
  has_many :users, through: :accounts
  has_many :income_items
  has_many :savings_accounts
  has_many :expense_categories
  has_many :debts
end
