class IncomeItem < ActiveRecord::Base
  belongs_to :budget
  
  def self.create_from_params(form_hash)
    if form_hash.blank? || form_hash.class.to_s != "ActiveSupport::HashWithIndifferentAccess" then
      return nil
    end
    
    item = IncomeItem.new
    item.name = form_hash[:incomeName]
    item.category = form_hash[:incomeCategory]
    item.recurring = (form_hash[:incomeRecurring] == "on")
    item.amount = form_hash[:incomeAmount].sub(/\$/, "").to_d
    
    #Add category for use in later budgets
    category = OtherCategory.where(user_id: user.id, name: item.category, category_type: "income")
    if category.first == nil then
      category = OtherCategory.new(name: item.category, category_type: "income")
      user.other_categories << category
      user.save
    end
    
    return item
  end
end