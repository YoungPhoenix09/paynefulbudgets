class Debt < ActiveRecord::Base
  belongs_to :budget
  has_many :debt_payments
  
  def self.create_from_params(form_hash)
    if form_hash.blank? || form_hash.class.to_s != "ActiveSupport::HashWithIndifferentAccess" then
      return nil
    end
    
    item = Debt.new
    item.name = debt[:debtName]
    item.amount_remaining = debt[:debtAmount].sub(/\$/, "").to_d
    item.current_amount_remaining = debt[:debtAmount].sub(/\$/, "").to_d
    
    return item
  end
end
