class ExpenseCategory < ActiveRecord::Base
  belongs_to :budget
  has_many :expenses
  
  def self.create_from_params(form_hash)
    if form_hash.blank? || form_hash.class.to_s != "ActiveSupport::HashWithIndifferentAccess" then
      return nil
    end
    
    item = ExpenseCategory.new
    item.name = expenseCategory[:expenseName]
    item.budgeted_amount = expenseCategory[:expenseAmount].sub(/\$/, "").to_d
    item.remaining_amount = expenseCategory[:expenseAmount].sub(/\$/, "").to_d
    
    return item
  end
end
