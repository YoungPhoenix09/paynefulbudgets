/*jslint white:true, nomen: true, plusplus: true */
/*global angular, $, console, document */

var app = angular.module("paynefulBudgets", []);

$(document).on('turbolinks:load', function () {
    'use strict';
    angular.bootstrap(document.body, ['paynefulBudgets']);
});

app.factory("currencyFormatter", function() {
    'use strict';
    return function(val) {
        var numStr = "";
        val = val.replace("$", "").replace(/,/g, "");

        if (!isNaN(parseFloat(val))) {
            numStr = "$" + parseFloat(val).toString();

            if (numStr.search("\\.") === -1) {
                numStr = numStr + ".00";
            } else if (numStr.search("\\.") !== numStr.length - 3) {
                numStr = numStr + "00";
                numStr = numStr.substr(0, numStr.search("\\.") + 3);
            }
        } else {
            numStr = "$0.00";
        }

        return numStr;
    };
});

app.controller("pbUtils", ['$scope', '$document', '$timeout', '$http', '$window',
    function ($scope, $document, $timeout, $http, $window) {
        'use strict';
        $scope.util = {
            deadLink: function (e) {
                return false;
            },
            timeoutVar: 0,
            warnTimeout: function () {
                $scope.util.timeoutVar = $timeout(function () {
                    console.log("1 minute before logout!");
                    $("#session-expire").css("display", "block");
                    $scope.util.timeout();
                    $document.off("click");
                }, 60000 * 14);
            },
            timeout: function () {
                $scope.util.timeoutVar = $timeout(function () {
                    var req = {
                        method: "GET",
                        url: "/authentication/logout",
                        headers: {
                            'Content-Type': 'text/html'
                        }
                    };

                    $http(req).then(function success() {
                        $window.location.reload();
                    });
                }, 60000);
            },
            resetTimeout: function () {
                $timeout.cancel($scope.util.timeoutVar);
                $("#session-expire").css("display", "none");
                $document.click(function () {
                    $timeout.cancel($scope.util.timeoutVar);
                    $scope.util.warnTimeout();
                });
            }

        };

        $document.ready(function () {
            console.log("Page up!  15 minutes before logout!");
            $scope.util.resetTimeout();
            $scope.util.warnTimeout();
        });
}]);

app.controller("pbCreateBudget", ["$scope", "currencyFormatter", function ($scope, currFmt) {
    'use strict';
    $scope.budget = {
        amountHandler: function (e) {
            console.log(e.currentTarget);
            var val = e.currentTarget.value;

            //Format to currency value using service
            val = currFmt(val);

            e.currentTarget.value = val;
        },
        incomeItems: {
            items: [{
                id: 1,
                name: "",
                category: "",
                recurring: false,
                amount: "$0.00"
            }],
            addLink: function () {
                angular.element("#income-modal").css("display", "block");
                $scope.budget.incomeItems.incomeItemAddNum = 0;
            },
            addButton: function () {
                angular.element("#income-modal").css("display", "none");

                var i, item, itemNum = $scope.budget.incomeItems.items.length;

                for (i = $scope.budget.incomeItems.incomeItemAddNum; i > 0; i--) {
                    item = {
                        id: itemNum + i,
                        name: "",
                        category: "",
                        recurring: false,
                        amount: "$0.00"
                    };

                    $scope.budget.incomeItems.items.push(item);
                }
            },
            rmvButton: function (obj) {
                var idx = $scope.budget.incomeItems.items.indexOf(obj);
                $scope.budget.incomeItems.items.splice(idx, 1);
            }
        },
        savingsItems: {
            items: [{
                id: 1,
                name: "",
                category: "",
                amount: "$0.00"
            }],
            addLink: function () {
                angular.element("#savings-modal").css("display", "block");
                $scope.budget.savingsItems.savingsItemAddNum = 0;
                return false;
            },
            addButton: function () {
                angular.element("#savings-modal").css("display", "none");

                var i, item, itemNum = $scope.budget.savingsItems.items.length;

                for (i = $scope.budget.savingsItems.savingsItemAddNum; i > 0; i--) {

                    item = {
                        id: itemNum + i,
                        name: "",
                        category: "",
                        amount: "$0.00"
                    };

                    $scope.budget.savingsItems.items.push(item);
                }
            },
            rmvButton: function (obj) {
                var idx = $scope.budget.savingsItems.items.indexOf(obj);
                $scope.budget.savingsItems.items.splice(idx, 1);
            }
        },
        expenseCategories: {
            items: [{
                id: 1,
                name: "",
                amount: "$0.00"
            }],
            addLink: function () {
                angular.element("#expense-modal").css("display", "block");
                $scope.budget.expenseCategories.expenseItemAddNum = 0;
                return false;
            },
            addButton: function () {
                angular.element("#expense-modal").css("display", "none");

                var i, item, itemNum = $scope.budget.expenseCategories.items.length;

                for (i = $scope.budget.expenseCategories.expenseItemAddNum; i > 0; i--) {
                    item = {
                        id: itemNum + i,
                        name: "",
                        amount: "$0.00"
                    };

                    $scope.budget.expenseCategories.items.push(item);
                }
            },
            rmvButton: function (obj) {
                var idx = $scope.budget.expenseCategories.items.indexOf(obj);
                $scope.budget.expenseCategories.items.splice(idx, 1);
            }
        },
        debts: {
            items: [{
                id: 1,
                name: "",
                amount: "$0.00"
            }],
            addLink: function () {
                angular.element("#debt-modal").css("display", "block");
                $scope.budget.debts.debtItemAddNum = 0;
                return false;
            },
            addButton: function () {
                angular.element("#debt-modal").css("display", "none");

                var i, item, itemNum = $scope.budget.debts.items.length;

                for (i = $scope.budget.debts.debtItemAddNum; i > 0; i--) {
                    item = {
                        id: itemNum + i,
                        name: "",
                        amount: "$0.00"
                    };

                    $scope.budget.debts.items.push(item);
                }
            },
            rmvButton: function (obj) {
                var idx = $scope.budget.debts.items.indexOf(obj);
                $scope.budget.debts.items.splice(idx, 1);
            }
        }
    };
}]);

app.controller("pbManageBudget", ["$scope", "currencyFormatter", function ($scope, currFmt) {
    'use strict';
    $scope.budget = {
        amountHandler: function (e) {
            console.log(e.currentTarget);
            var val = e.currentTarget.value;

            //Format to currency value using service
            val = currFmt(val);

            e.currentTarget.value = val;
        },
        amountFormatter: function (val) {
            var numStr = "";
            val = val.replace("$", "").replace(/,/g, "");

            if (!isNaN(parseFloat(val))) {
                numStr = "$" + parseFloat(val).toString();

                if (numStr.search("\\.") === -1) {
                    numStr = numStr + ".00";
                } else if (numStr.search("\\.") !== numStr.length - 3) {
                    numStr = numStr + "00";
                    numStr = numStr.substr(0, numStr.search("\\.") + 3);
                }
            } else {
                numStr = "$0.00";
            }

            return numStr;
        },
        accordions: {
            savings: {
                buttonCaption: "Add Deposit",
                open: false,
                switch: function () {
                    $scope.budget.accordions.savings.open = !$scope.budget.accordions.savings.open;
                },
                childSwitch: function (e) {
                    var button, content;
                    
                    button = e.currentTarget;
                    content = $(button.parentElement.parentElement.parentElement.parentElement.nextElementSibling);
                    
                    content.toggleClass("w3-show");
                    $(button).toggleClass("expanded");
                }
            },
            expenses: {
                buttonCaption: "Add Expense",
                open: false,
                switch: function () {
                    $scope.budget.accordions.expenses.open = !$scope.budget.accordions.expenses.open;
                },
                childSwitch: function (e) {
                    var button, content;
                    
                    button = e.currentTarget;
                    content = $(button.parentElement.parentElement.parentElement.parentElement.nextElementSibling);
                    
                    content.toggleClass("w3-show");
                    $(button).toggleClass("expanded");
                }
            },
            debts: {
                buttonCaption: "Add Payment",
                open: false,
                switch: function () {
                    $scope.budget.accordions.debts.open = !$scope.budget.accordions.debts.open;
                },
                childSwitch: function (e) {
                    var button, content;
                    
                    button = e.currentTarget;
                    content = $(button.parentElement.parentElement.parentElement.parentElement.nextElementSibling);
                    
                    content.toggleClass("w3-show");
                    $(button).toggleClass("expanded");
                }
            }
        },
        modals: {
            deposit: {
                add_edit: {
                    header: "Add",
                    btn: "Add",
                    id_num: null,
                    acctVisible: "true",
                    show: function () {
                        $("select[name=savAccount]")[0].value = null;
                        $("#deposit_trans_date").val(null);
                        $("#deposit_amount").val("$0.00");

                        angular.element("#add-deposit-modal").css("display", "block");
                    },
                    showEdit: function (e) {
                        $scope.budget.modals.deposit.add_edit.id_num = parseInt(e.currentTarget.id.replace("editDep", ""), 10);
                        $scope.budget.modals.deposit.add_edit.header = "Edit";
                        $scope.budget.modals.deposit.add_edit.btn = "Update";
                        $scope.budget.modals.deposit.add_edit.acctVisible = false;
                        $.get("/savings_deposits/get_deposit_details", {
                            id: $scope.budget.modals.deposit.add_edit.id_num
                        }, function (data) {
                            console.log(data);

                            var account = e.currentTarget.attributes["data-account"].value,
                                amount = currFmt(data.amount.toString());

                            $("select[name=savAccount]")[0].value = account;
                            $("#deposit_trans_date").val(data.trans_date.substr(0, 10));
                            $("#deposit_amount").val(amount);

                            angular.element("#add-deposit-modal").css("display", "block");
                        }, "json");
                    },
                    hide: function () {
                        angular.element("#add-deposit-modal").css("display", "none");
                        $scope.budget.modals.deposit.add_edit.id_num = null;
                        $scope.budget.modals.deposit.add_edit.acctVisible = true;
                        $scope.budget.modals.deposit.add_edit.header = "Add";
                        $scope.budget.modals.deposit.add_edit.btn = "Add";
                    }
                },
                remove: {
                    id_num: null,
                    acctName: "",
                    depositAmt: "",
                    show: function (e) {
                        var elem = e.currentTarget,
                            dep_id = parseInt(elem.id.replace("rmvDep", ""), 10);

                        $scope.budget.modals.deposit.remove.id_num = dep_id;
                        $scope.budget.modals.deposit.remove.acctName = elem.attributes["data-account"].value;
                        $scope.budget.modals.deposit.remove.depositAmt = elem.attributes["data-amount"].value;

                        angular.element("#rmv-deposit-modal").css("display", "block");
                    },
                    hide: function () {
                        angular.element("#rmv-deposit-modal").css("display", "none");
                        $scope.budget.modals.deposit.remove.id_num = null;
                        $scope.budget.modals.deposit.remove.acctName = null;
                        $scope.budget.modals.deposit.remove.depositAmt = null;
                    }
                }
            },
            expense: {
                add_edit: {
                    header: "Add",
                    btn: "Add",
                    id_num: null,
                    categoryVisible: "true",
                    show: function () {
                        $("select[name=expCategory]")[0].value = null;
                        $("#expense_name").val("");
                        $("#expense_trans_date").val(null);
                        $("#expense_recurring")[0].checked = false;
                        $("#expense_amount").val("$0.00");

                        angular.element("#add-expense-modal").css("display", "block");
                    },
                    showEdit: function (e) {
                        $scope.budget.modals.expense.add_edit.id_num = parseInt(e.currentTarget.id.replace("editExp", ""), 10);
                        $scope.budget.modals.expense.add_edit.header = "Edit";
                        $scope.budget.modals.expense.add_edit.btn = "Update";
                        $scope.budget.modals.expense.add_edit.categoryVisible = false;
                        $.get("/expense/get_expense_details", {
                            id: $scope.budget.modals.expense.add_edit.id_num
                        }, function (data) {
                            console.log(data);

                            var category = e.currentTarget.attributes["data-category"].value,
                                amount = currFmt(data.amount.toString());

                            $("select[name=expCategory]")[0].value = category;
                            $("#expense_name").val(data.name);
                            $("#expense_trans_date").val(data.trans_date.substr(0, 10));
                            $("#expense_recurring")[0].checked = data.recurring;
                            $("#expense_amount").val(amount);

                            angular.element("#add-expense-modal").css("display", "block");
                        }, "json");
                    },
                    hide: function () {
                        angular.element("#add-expense-modal").css("display", "none");
                        $scope.budget.modals.expense.add_edit.id_num = null;
                        $scope.budget.modals.expense.add_edit.categoryVisible = true;
                        $scope.budget.modals.expense.add_edit.header = "Add";
                        $scope.budget.modals.expense.add_edit.btn = "Add";
                    }
                },
                remove: {
                    id_num: null,
                    catName: "",
                    expName: "",
                    show: function (e) {
                        var elem = e.currentTarget,
                            exp_id = parseInt(elem.id.replace("rmvExp", ""), 10);

                        $scope.budget.modals.expense.remove.id_num = exp_id;
                        $scope.budget.modals.expense.remove.catName = elem.attributes["data-category"].value;
                        $scope.budget.modals.expense.remove.expName = elem.attributes["data-expense"].value;

                        angular.element("#rmv-expense-modal").css("display", "block");
                    },
                    hide: function () {
                        angular.element("#rmv-expense-modal").css("display", "none");
                        $scope.budget.modals.expense.remove.id_num = null;
                        $scope.budget.modals.expense.remove.catName = null;
                        $scope.budget.modals.expense.remove.expName = null;
                    }
                }
            },
            payment: {
                add_edit: {
                    header: "Add",
                    btn: "Add",
                    id_num: null,
                    debtVisible: "true",
                    show: function () {
                        $("select[name=debt]")[0].value = null;
                        $("#payment_trans_date").val(null);
                        $("#payment_recurring")[0].checked = false;
                        $("#payment_amount").val("$0.00");

                        angular.element("#add-payments-modal").css("display", "block");
                    },
                    showEdit: function (e) {
                        $scope.budget.modals.payment.add_edit.id_num = parseInt(e.currentTarget.id.replace("editPymt", ""), 10);
                        $scope.budget.modals.payment.add_edit.header = "Edit";
                        $scope.budget.modals.payment.add_edit.btn = "Update";
                        $scope.budget.modals.payment.add_edit.debtVisible = false;
                        $.get("/debt_payments/get_payment_details", {
                            id: $scope.budget.modals.payment.add_edit.id_num
                        }, function (data) {
                            console.log(data);

                            var debtName = e.currentTarget.attributes["data-debt"].value,
                                amount = currFmt(data.amount.toString());

                            $("select[name=debt]")[0].value = debtName;
                            $("#payment_trans_date").val(data.trans_date.substr(0, 10));
                            $("#payment_recurring")[0].checked = data.recurring;
                            $("#payment_amount").val(amount);

                            angular.element("#add-payments-modal").css("display", "block");
                        }, "json");
                    },
                    hide: function () {
                        angular.element("#add-payments-modal").css("display", "none");
                        $scope.budget.modals.payment.add_edit.id_num = null;
                        $scope.budget.modals.payment.add_edit.debtVisible = true;
                        $scope.budget.modals.payment.add_edit.header = "Add";
                        $scope.budget.modals.payment.add_edit.btn = "Add";
                    }
                },
                remove: {
                    id_num: null,
                    debtName: "",
                    paymentAmt: "",
                    show: function (e) {
                        var elem = e.currentTarget,
                            pymt_id = parseInt(elem.id.replace("rmvPymt", ""), 10);

                        $scope.budget.modals.payment.remove.id_num = pymt_id;
                        $scope.budget.modals.payment.remove.debtName = elem.attributes["data-debt"].value;
                        $scope.budget.modals.payment.remove.paymentAmt = elem.attributes["data-amount"].value;

                        angular.element("#rmv-payments-modal").css("display", "block");
                    },
                    hide: function () {
                        angular.element("#rmv-payments-modal").css("display", "none");
                        $scope.budget.modals.payment.remove.id_num = null;
                        $scope.budget.modals.payment.remove.debtName = null;
                        $scope.budget.modals.payment.remove.paymentAmt = null;
                    }
                }
            }
        }
    };
}]);

app.directive("pbBudgetRtrv", ["$http", "currencyFormatter", function ($http, currFmt) {
    /*jshint validthis:true */
    'use strict';
    
    function getData(scope, element, attrs) {
        console.log("Getting data for budget id " + attrs.budgetId);
        var success, config = {
            headers: {
                "Accept": "application/json"
            },
            responseType: "json"
        };
        
        success = angular.bind(this, function (domBudget, response) {
            var item, income, incomeItems = [], acct, savingsAccts = [], cat, expenseCats = [], debt, debts = [], budget = response.data;
            
            
            //Create income items
            for (income in budget.income_items) {
                if (budget.income_items.hasOwnProperty(income)) {
                    item = {
                        id: budget.income_items[income].id,
                        name: budget.income_items[income].name,
                        category: budget.income_items[income].category,
                        recurring: budget.income_items[income].recurring,
                        amount: currFmt(budget.income_items[income].amount)
                    };

                    incomeItems.push(item);
                }
            }
            domBudget.incomeItems.items = incomeItems;
            
            //Create savings accounts
            for (acct in budget.savings_accounts) {
                if (budget.savings_accounts.hasOwnProperty(acct)) {
                    item = {
                        id: budget.savings_accounts[acct].id,
                        name: budget.savings_accounts[acct].name,
                        category: budget.savings_accounts[acct].category,
                        amount: currFmt(budget.savings_accounts[acct].amount_deposited)
                    };

                    savingsAccts.push(item);
                }
            }
            domBudget.savingsItems.items = savingsAccts;
            
            //Create expense categories
            for (cat in budget.expense_categories) {
                if (budget.expense_categories.hasOwnProperty(cat)) {
                    item = {
                        id: budget.expense_categories[cat].id,
                        name: budget.expense_categories[cat].name,
                        amount: currFmt(budget.expense_categories[cat].budgeted_amount)
                    };

                    expenseCats.push(item);
                }
            }
            domBudget.expenseCategories.items = expenseCats;
            
            //Create debts
            for (debt in budget.debts) {
                if (budget.debts.hasOwnProperty(debt)) {
                    item = {
                        id: budget.debts[debt].id,
                        name: budget.debts[debt].name,
                        amount: currFmt(budget.debts[debt].amount_remaining)
                    };

                    debts.push(item);
                }
            }
            domBudget.debts.items = debts;
            
            console.log(domBudget);
            console.log(budget);
            
        }, scope.domBudget);
        
        $http.get("/budgets/get_budget_details/" + attrs.budgetId, config).then(success, function (response) {
            console.log(response);
        });
    }
    
    return {
        link: getData,
        scope: {
            budgetId: "=",
            domBudget: "="
        }
    };
}]);

app.directive("budgetAccordion", function () {
    'use strict';
    
    return {
        templateUrl: "/tmpAccordion.html",
        transclude: true,
        link: function (scope, element, attrs) {
            console.log(attrs);
            
            scope.header1 = attrs.headerOne;
            scope.header2 = attrs.headerTwo;
            scope.title = attrs.headerTitle;
            scope.color = (!!attrs.headerColor) ? attrs.headerColor : "#4388CC";
            
            console.log(scope);
        },
        restrict: "E",
        scope: {
            section: "=",
            modal: "=addButtonModal"
        }
    };
});