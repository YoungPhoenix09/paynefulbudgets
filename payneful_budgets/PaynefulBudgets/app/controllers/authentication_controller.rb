class AuthenticationController < ApplicationController
  layout "auth"
  
  def login
    if params[:commit] == nil then
      render "login"
      return
    end
  end

  def authenticate
    user = params[:username]
    pass = params[:password]
    
    user = User.find_by_user_name(user)
    if user && user == user.authenticate(pass) then
      session[:user] = user.user_name
      session[:name] = user.first_name + " " + user.last_name
      
      user.last_login = Time.now
      user.save
      
      redirect_to controller: "main", action: "index"
      return
    else
      #Show err
      flash.now[:error] = "Invalid crendentials.  Please try again."
      render "login"
    end
  end

  def logout
    session[:user] = nil
    session[:name] = nil
    redirect_to action: "login"
  end

  def new_user
    if params[:commit] == nil then
      render "new_user"
      return
    end
    
    if params[:username] != "" && params[:password] != "" then
      #Verify Username starts with letter, contains only alphanumerics, and is between 6-16 characters
      if params[:username].match('^[A-Za-z](\d|\w){5,16}') == nil then
        flash.now[:error] = "Username must start with a letter, contain only alphanumeric characters, and be 6-16 characters long."
        render "new_user"
        return
      end
      
      #Verify first name
      if params[:first_name].match('^([A-Za-z]|\'|,){1,32}$') == nil then
        flash.now[:error] = "Invalid characters in first name."
        render "new_user"
        return
      end
      
      #Verify last name
      if params[:last_name].match('^([A-Za-z]|\'|,){1,32}$') == nil then
        flash.now[:error] = "Invalid characters in last name."
        render "new_user"
        return
      end
      
      #Verify Password
      if params[:password].match('[A-Z]') == nil || params[:password].match('[^\s\w]') == nil || params[:password].match('^\S{8,16}$') == nil then
        flash.now[:error] = "Password must be 8-16 characters, have at least 1 capital letter, and at least 1 special character."
        render "new_user"
        return
      end
      if params[:password] != params[:confirm] then
        flash.now[:error] = "Passwords do not match."
        render "new_user"
        return
      end
      
      #Create user
      user = User.new
      user.user_name = params[:username]
      user.password = params[:password]
      user.first_name = params[:first_name]
      user.last_name = params[:last_name]
      user.last_login = Time.now
      user.save
      
      #Update session and send to main
      session[:user] = user.user_name
      session[:name] = user.first_name + " " + user.last_name
      
      redirect_to controller: "main", action: "index"
    else
      flash.now[:error] = "Please make sure you properly fill out all the fields."
      render "new_user"
    end
  end
  
end
