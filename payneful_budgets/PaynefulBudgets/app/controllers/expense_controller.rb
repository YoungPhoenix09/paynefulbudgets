class ExpenseController < ApplicationController
  def get_expense_details
    if !session_handler(session[:user]) || params[:id].blank? then
      case
      when !session_handler(session[:user])
        respond_to do |format|
          format.json {render :json => "Invalid authentication!" }
        end
      when params[:id].blank?
        respond_to do |format|
          format.json {render :json => "No expense id provided!" }
        end
      end
      
      return
    end
    
    respond_to do |format|
      format.json {render :json => Expense.find(params[:id]).to_json }
    end  
  end

  def add_expense
    if !session_handler(session[:user]) then
      return
    end
    
    if params[:expense].blank? || params[:budget_id].blank? || params[:expCategory].blank? then
      raise
    end
    
    if !params[:expense_id].blank? then
      update_expense params[:expense_id]
      
      flash[:notice] = "Expense successfully updated!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
      return
    end
    
    #Get expense category the expense should be tied to
    exp_cat = ExpenseCategory.where(name: params[:expCategory], budget_id: params[:budget_id]).first()
    if exp_cat == nil then
      raise
    end
    
    #Reformat amount to float
    params[:expense][:amount] = params[:expense][:amount].sub("$","").to_f
    
    #Create expense
    exp = Expense.new params.require(:expense).permit(:name, :trans_date, :recurring, :amount)
    result = exp.save
    
    if !result then
      raise
    end
    
    #Associate with expense category
    exp_cat.expenses << exp
    exp_cat.remaining_amount = exp_cat.remaining_amount - exp.amount
    result = exp_cat.save
    
    if !result then
      raise
    end
    
    flash[:notice] = "Expense successfully added!"
    redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  rescue => e
      puts e.message
      flash[:error] = "Expense could not be added!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  end
  
  def update_expense(exp_id)
    #Find expense
    exp = Expense.find(exp_id)
    
    exp.name = params[:expense][:name]
    exp.recurring = params[:expense][:recurring]
    exp.trans_date = params[:expense][:trans_date]
    
    #Update Expense Category amounts if changed
    amount = params[:expense][:amount].sub("$","").to_f
    if amount != exp.amount then
      exp_cat = exp.expense_category
      exp_cat.remaining_amount = exp_cat.remaining_amount + exp.amount
      
      exp.amount = amount
      exp.save
      
      exp_cat.remaining_amount = exp_cat.remaining_amount - exp.amount
      exp_cat.save
    else
      exp.save
    end
    
  end

  def remove_expense
    if !session_handler(session[:user]) then
      return
    end
    
    expense = Expense.find(params[:expense_id])
    
    exp_cat = expense.expense_category
    exp_cat.remaining_amount = exp_cat.remaining_amount + expense.amount
    exp_cat.save
    
    expense.delete
    
    flash[:notice] = "Expense successfully removed!"
    redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  rescue => e
    puts e.message
    flash[:error] = "Failed to remove expense!"
    redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  end
  
  def session_handler(user_name)
      if user_name == nil then
        redirect_to controller: "authentication", action: "login"
        return false
      end
      
      user = User.find_by_user_name user_name
      if user == nil
        session['user'] = nil
        session['name'] = nil
        redirect_to controller: "authentication", action: "login"
        return false
      end
      
      return true
  end
end
