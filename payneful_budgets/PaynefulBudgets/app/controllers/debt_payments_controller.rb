class DebtPaymentsController < ApplicationController
  def add_payment
      if !session_handler(session[:user]) || params[:commit] == nil then
          return
      end
      
      if !params[:payment_id].blank? then
        update_payment params[:payment_id]
        
        flash[:notice] = "Payment successfully updated!"
        redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
        return
      else
        #Find debt
        debt = Debt.where(budget: params[:budget_id], name: params[:debt]).first
        
        #Reformat amount to float
        params[:payment][:amount] = params[:payment][:amount].sub("$","").to_f
        
        #Create new debt payment
        payment = DebtPayment.new params.require(:payment).permit(:recurring, :amount, :trans_date)
        payment.save
        
        #Associate payment with debt
        debt.debt_payments << payment
        debt.current_amount_remaining = debt.current_amount_remaining - payment.amount
        debt.save
      end
      
      flash[:notice] = "Payment successfully added!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  rescue => err
      puts err
      
      flash[:error] = "Failed to add payment..."
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  end
  
  def update_payment(pymt_id)
    #Find Debt Payment
    pymt = DebtPayment.find(pymt_id)
    
    pymt.recurring = params[:payment][:recurring]
    pymt.trans_date = params[:payment][:trans_date]
    
    #Update Debt amounts if changed
    amount = params[:payment][:amount].sub("$","").to_f
    if amount != pymt.amount then
      debt = pymt.debt
      debt.current_amount_remaining = debt.current_amount_remaining + pymt.amount
      
      pymt.amount = amount
      pymt.save
      
      debt.current_amount_remaining = debt.current_amount_remaining - pymt.amount
      debt.save
    else
      pymt.save
    end
    
  end

  def remove_payment
      if !session_handler(session[:user]) || params[:commit] == nil then
          return
      end
      
      pymt = DebtPayment.find(params[:payment_id])
    
      debt = pymt.debt
      debt.current_amount_remaining = debt.current_amount_remaining + pymt.amount
      debt.save
      
      pymt.delete
      
      flash[:notice] = "Payment successfully removed!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  rescue => e
      puts e.message
      flash[:error] = "Failed to remove payment!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  end

  def get_payment_details
      if !session_handler(session[:user]) || params[:id].blank? then
        case
        when !session_handler(session[:user])
          respond_to do |format|
            format.json {render :json => "Invalid authentication!" }
          end
        when params[:id].blank?
          respond_to do |format|
            format.json {render :json => "No payment id provided!" }
          end
        end
        
        return
      end
    
      respond_to do |format|
        format.json {render :json => DebtPayment.find(params[:id]).to_json }
      end  
  end
      
  def session_handler(user_name)
      if user_name == nil then
        redirect_to controller: "authentication", action: "login"
        return false
      end
      
      user = User.find_by_user_name user_name
      if user == nil
        session['user'] = nil
        session['name'] = nil
        redirect_to controller: "authentication", action: "login"
        return false
      end
      
      return true
  end
end
