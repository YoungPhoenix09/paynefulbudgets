class MainController < ApplicationController
  @budgets = nil
  
  def index
    #Session Handler
    if session['user'] == nil then
      redirect_to controller: "authentication", action: "login"
      return
    end
    
    user = User.find_by_user_name session['user']
    if user == nil
      session['user'] = nil
      session['name'] = nil
      redirect_to controller: "authentication", action: "login"
      return
    end
    
    @budgets = user.budgets
    render 'index'
  end
  
  def test_json
    respond_to do |format|
      format.json {render :json => User.find_by_user_name("YoungPhoenix09").to_json }
    end
  end
end
