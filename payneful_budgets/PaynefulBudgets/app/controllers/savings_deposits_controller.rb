class SavingsDepositsController < ApplicationController
  def add_deposit
      if !session_handler(session[:user]) || params[:commit] == nil then
          return
      end
      
      if !params[:deposit_id].blank? then
        update_deposit params[:deposit_id]
        
        flash[:notice] = "Deposit successfully updated!"
        redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
        return
      else
        #Find savings account
        acct = SavingsAccount.where(budget: params[:budget_id], name: params[:savAccount]).first
        
        #Reformat amount to float
        params[:deposit][:amount] = params[:deposit][:amount].sub("$","").to_f
        
        #Create new savings deposit
        deposit = SavingsDeposit.new params.require(:deposit).permit(:amount, :trans_date)
        deposit.save
        
        #Associate deposit with account
        acct.savings_deposits << deposit
        acct.current_amount_deposited = acct.current_amount_deposited + deposit.amount
        acct.save
      end
      
      flash[:notice] = "Deposit successfully added!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  rescue => err
      puts err
      
      flash[:error] = "Failed to add deposit..."
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  end
  
  def update_deposit(dep_id)
    #Find Savings Deposit
    deposit = SavingsDeposit.find(dep_id)
    
    deposit.trans_date = params[:deposit][:trans_date]
    
    #Update Savings Account amounts if changed
    amount = params[:deposit][:amount].sub("$","").to_f
    if amount != deposit.amount then
      acct = deposit.savings_account
      acct.current_amount_deposited = acct.current_amount_deposited - deposit.amount
      
      deposit.amount = amount
      deposit.save
      
      acct.current_amount_deposited = acct.current_amount_deposited + deposit.amount
      acct.save
    else
      deposit.save
    end
    
  end

  def remove_deposit
    if !session_handler(session[:user]) || params[:commit] == nil then
          return
      end
      
      deposit = SavingsDeposit.find(params[:deposit_id])
    
      acct = deposit.savings_account
      acct.current_amount_deposited = acct.current_amount_deposited - deposit.amount
      acct.save
      
      deposit.delete
      
      flash[:notice] = "Deposit successfully removed!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  rescue => e
      puts e.message
      flash[:error] = "Failed to remove deposit!"
      redirect_to controller: "budgets", action: "manage_budget", id: params[:budget_id]
  end

  def get_deposit_details
      authenticated = session_handler(session[:user])
      
      if !authenticated || params[:id].blank? then
        case
        when !authenticated
          return 
        when params[:id].blank?
          respond_to do |format|
            format.json {render :json => "No deposit id provided!" }
          end
        end
        
        return
      end
    
      respond_to do |format|
        format.json {render :json => SavingsDeposit.find(params[:id]).to_json }
      end
  rescue => e
      puts e
      
      respond_to do |format|
        format.json {render :json => "No deposit found!" }
      end
  end
  
  def session_handler(user_name)
      if user_name == nil then
        respond_to do |format|
          format.html {redirect_to controller: "authentication", action: "login"}
          format.json {render :json => "Invalid authentication!" }
        end
        
        return false
      end
      
      user = User.find_by_user_name user_name
      if user == nil
        session['user'] = nil
        session['name'] = nil
        respond_to do |format|
          format.html {redirect_to controller: "authentication", action: "login"}
          format.json {render :json => "Invalid authentication!" }
        end
        return false
      end
      
      return true
  end
end
