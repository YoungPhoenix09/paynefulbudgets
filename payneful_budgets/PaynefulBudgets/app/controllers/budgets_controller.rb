class BudgetsController < ApplicationController
  
  def self.currFmt val
      numStr = ""
      
      #Add hundredths-place zero if necessary
      val = val.to_s.gsub(/$/, "").gsub(/,/, "")
      numStr = val.to_f.to_s 
      numStr = numStr + "0" if val.index(/\./) != val.size - 3
      
      #Format commas
      first_comma_index = numStr.index(/\./) #Start from period
      while first_comma_index - 3 >= 1 
          numStr.insert(first_comma_index - 3, ",")
          first_comma_index = numStr.index(/,/)
      end
      
      #Add dollar sign
      numStr = "$" + numStr
      
      return numStr
  end
    
  def create_budget
    if !session_handler(session[:user]) || params[:commit] == nil then
      return
    end
    
    #Get current user
    user = User.find_by_user_name(session[:user])
    
    #Create new budget object
    budget = Budget.new
    budget.month = params[:date][:month]
    budget.year = params[:date][:year]
    budget.save
    
    #Tie to account
    account = Account.new
    account.owner = true
    account.user = user
    account.budget = budget
    account.save
    
    #Add income items
    if params[:incomeItems] != nil && params[:incomeItems].length > 0 then
      params[:incomeItems].each_value { |incomeItem|
        item = IncomeItem.create_from_params(incomeItem)
        item.save
        budget.income_items << item
      }
      budget.save
    end
    
    #Add savings accounts
    if params[:savingsItems] != nil && params[:savingsItems].length > 0 then
      params[:savingsItems].each_value { |savingsItem|
        item = SavingsAccount.create_from_params(savingsItem)
        item.save
        budget.savings_accounts << item
      }
      budget.save
    end
    
    #Add Expense categories
    if params[:expenseCategories] != nil && params[:expenseCategories].length > 0 then
      params[:expenseCategories].each_value { |expenseCategory|
        item = ExpenseCategory.create_from_params(expenseCategory)
        item.save
        budget.expense_categories << item
      }
      budget.save
    end
    
    #Add debts
    if params[:debts] != nil && params[:debts].length > 0 then
      params[:debts].each_value { |debt|
        item = Debt.create_from_params(debt)
        item.save
        budget.debts << item
      }
      budget.save
    end
    
    #Show budget management page
    redirect_to action: "manage_budget", id: budget.id
  end

  def manage_budget
    if !session_handler(session[:user]) then
      return
    end
    
    @budget = Budget.find(params[:id])
    @metrics = {
      :currentIncome => 0.00,
      :currentBalance => 0.00,
      :expenses => {
        :ttlAmtAllocated => 0.00,
        :ttlAmtSpent => 0.00
      },
      :savings => {
        :ttlAmtInvested => 0.00,
        :ttlAmtDeposited => 0.00
      },
      :debt => {
        :ttlDebtRemaining => 0.00,
        :ttlDebtPaid => 0.00
      }
    }
    
    #Calculate Current Income
    currentIncome = 0.00
    @budget.income_items.each do |item|
      currentIncome = currentIncome + item.amount
    end
    @metrics[:currentIncome] = currentIncome
    
    #Calculate Savings Metrics
    ttlAmtInvested = 0.00
    ttlAmtDeposited = 0.00
    @budget.savings_accounts.each do |acct|
      ttlAmtInvested = ttlAmtInvested + acct.current_amount_deposited
      
      acct.savings_deposits.each do |deposit|
        ttlAmtInvested = ttlAmtInvested + deposit.amount
        ttlAmtDeposited = ttlAmtDeposited + deposit.amount
      end
    end
    @metrics[:savings][:ttlAmtInvested] = ttlAmtInvested
    @metrics[:savings][:ttlAmtDeposited] = ttlAmtDeposited
    
    #Calculate Expense Metrics
    ttlAmtAllocated = 0.00
    ttlAmtSpent = 0.00
    @budget.expense_categories.each do |cat|
      ttlAmtAllocated = ttlAmtAllocated + cat.budgeted_amount
      
      amtSpent = cat.budgeted_amount - cat.remaining_amount
      ttlAmtSpent = ttlAmtSpent + amtSpent
    end
    @metrics[:expenses][:ttlAmtAllocated] = ttlAmtAllocated
    @metrics[:expenses][:ttlAmtSpent] = ttlAmtSpent
    
    #Calculate Debt Metrics
    ttlDebtRemaining = 0.00
    ttlDebtPaid = 0.00
    @budget.debts.each do |debt|
      ttlDebtRemaining = ttlDebtRemaining + debt.current_amount_remaining
      
      debt.debt_payments.each do |pymt|
        ttlDebtPaid = ttlDebtPaid + pymt.amount
      end
    end
    ttlDebtRemaining = ttlDebtRemaining - ttlDebtPaid
    @metrics[:debt][:ttlDebtRemaining] = ttlDebtRemaining
    @metrics[:debt][:ttlDebtPaid] = ttlDebtPaid
    
    #Calculate Balance
    @metrics[:currentBalance] = @metrics[:currentIncome] - @metrics[:expenses][:ttlAmtSpent]
  end
  
  def edit_budget
    if !session_handler(session[:user]) then
      return
    end
    
    @budget = Budget.find(params[:id])
    
    if params[:commit].blank? then
      render :create_budget
      return
    end
    
    #Check income items
    found_income_items = {}
    params[:incomeItems].each_value do |item|
      #Add item to list
      found_income_items[item[:incomeName].to_sym] = true
      
      existing_income_item = IncomeItem.where(:name => item[:incomeName], :budget => @budget).first
      if existing_income_item.blank? then
        #Create record if does not exist
        new_income_item = IncomeItem.create_from_params(item)
        @budget.income_items << new_income_item
      else
        #Update record if exists
        existing_income_item.name = item[:incomeName]
        existing_income_item.category = item[:incomeCategory]
        existing_income_item.recurring = (item[:incomeRecurring] == "on")
        existing_income_item.amount = item[:incomeAmount].sub(/\$/, "").to_d
        existing_income_item.save
      end
    end
    #Remove existing that weren't kept
    @budget.income_items.each do |item|
      if !found_income_items[item.name.to_sym] then
        item.delete
      end
    end
    
    #Check savings accounts
    found_savings_accts = {}
    params[:savingsItems].each_value do |item|
      #Add item to list
      found_savings_accts[item[:savingsName].to_sym] = true
      
      existing_savings_accts = SavingsAccount.where(:name => item[:savingsName], :budget => @budget).first
      if existing_savings_accts.blank? then
        #Create record if does not exist
        new_savings_acct = SavingsAccount.create_from_params(item)
        @budget.savings_accounts << new_savings_acct
      else
        #Update record if exists
        existing_savings_accts.name = item[:savingsName]
        existing_savings_accts.category = item[:savingsCategory]
        existing_savings_accts.amount_deposited = item[:savingsAmount].sub(/\$/, "").to_d
        existing_savings_accts.current_amount_deposited = item[:savingsAmount].sub(/\$/, "").to_d
        existing_savings_accts.save
      end
    end
    #Remove existing that weren't kept
    @budget.savings_accounts.each do |acct|
      if !found_savings_accts[acct.name.to_sym] then
        acct.delete
      end
    end
    
    #Check expense categories
    found_expense_cats = {}
    params[:expenseCategories].each_value do |item|
      #Add item to list
      found_expense_cats[item[:expenseName].to_sym] = true
      
      existing_expense_cats = ExpenseCategory.where(:name => item[:expenseName], :budget => @budget).first
      if existing_expense_cats.blank? then
        #Create record if does not exist
        new_expense_cat = ExpenseCategory.create_from_params(item)
        @budget.expense_categories << new_expense_cat
      else
        #Update record if exists
        existing_expense_cats = ExpenseCategory.new
        existing_expense_cats.name = item[:expenseName]
        existing_expense_cats.budgeted_amount = item[:expenseAmount].sub(/\$/, "").to_d
        existing_expense_cats.remaining_amount = item[:expenseAmount].sub(/\$/, "").to_d
        existing_expense_cats.save
      end
    end
    #Remove existing that weren't kept
    @budget.expense_categories.each do |cat|
      if !found_expense_cats[cat.name.to_sym] then
        cat.delete
      end
    end
    
    #Check debts
    found_debts = {}
    params[:debts].each_value do |item|
      #Add item to list
      found_debts[item[:debtName].to_sym] = true
      
      existing_debts = Debt.where(:name => item[:debtName], :budget => @budget).first
      if existing_debts.blank? then
        #Create record if does not exist
        new_debt = Debt.create_from_params(item)
        @budget.debts << new_debt
      else
        #Update record if exists
        existing_debts = Debt.new
        existing_debts.name = item[:debtName]
        existing_debts.amount_remaining = item[:debtAmount].sub(/\$/, "").to_d
        existing_debts.current_amount_remaining = item[:debtAmount].sub(/\$/, "").to_d
        existing_debts.save
      end
    end
    #Remove existing that weren't kept
    @budget.debts.each do |debt|
      if !found_debts[debt.name.to_sym] then
        debt.delete
      end
    end
    
    @budget.save
    
    #Show budget management page
    redirect_to action: "manage_budget", id: @budget.id
  end

  def get_budget_details
    if !session_handler(session[:user]) then
      return
    end
    
    budget = Budget.find(params[:id])
    respond_to do |format|
      format.json {
        render :json => budget.to_json(include: [:income_items, :savings_accounts, :expense_categories, :debts])
      }
    end
    
  rescue => e
    puts e
    respond_to do |format|
      format.json {
        render :json => {:err => "Budget not found!"}.to_json
      }
    end
  end

  def remove_budget
    if !session_handler(session[:user]) then
      return
    end
    
    budget = Budget.find(params[:id])
    
    #Remove expenses
    budget.expense_categories.each do | cat |
      cat.expenses.each do |exp|
        exp.delete
      end
      
      cat.delete
    end
    
    #Remove savings
    budget.savings_accounts.each do | sav |
      sav.savings_deposits.each do | dep |
        dep.delete
      end
      
      sav.delete
    end
    
    #Remove debt
    budget.debts.each do | debt |
      debt.debt_payments.each do | pay |
        pay.delete
      end
      
      debt.delete
    end
    
    #Remove income
    budget.income_items.each do | item |
      item.delete
    end
    
    #Remove budget
    budget.delete
    
    redirect_to controller: "main", action: "index"
  end

  def show_budgets
    if !session_handler(session[:user]) then
      return
    end
  end

  def session_handler(user_name)
      if user_name == nil then
        respond_to do |format|
          format.html {
            redirect_to controller: "authentication", action: "login"
          }
          format.json {
            render :json => "Invalid authentication!"
          }
        end
        return false
      end
      
      user = User.find_by_user_name user_name
      if user == nil
        session['user'] = nil
        session['name'] = nil
        respond_to do |format|
          format.html {
            redirect_to controller: "authentication", action: "login"
          }
          format.json {
            render :json => "Invalid authentication!"
          }
        end
        return false
      end
      
      return true
  end
end
