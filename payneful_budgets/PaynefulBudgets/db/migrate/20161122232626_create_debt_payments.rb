class CreateDebtPayments < ActiveRecord::Migration
  def change
    create_table :debt_payments do |t|
      t.boolean :recurring
      t.decimal :amount
      t.datetime :trans_date
      t.references :debt, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
