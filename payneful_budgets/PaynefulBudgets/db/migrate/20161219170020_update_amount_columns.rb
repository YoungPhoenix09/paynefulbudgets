class UpdateAmountColumns < ActiveRecord::Migration
  def up
    change_table :expense_categories do |t|
      t.decimal :remaining_amount
    end
    
    change_table :debts do |t|
      t.decimal :current_amount_remaining
    end
    
    change_table :savings_accounts do |t|
      rename_column :savings_accounts, :current_amount, :current_amount_deposited
      t.decimal :amount_deposited
    end
  end
  
  def down
    change_table :expense_categories do |t|
      t.remove :remaining_amount
    end
    
    change_table :debts do |t|
      t.remove :current_amount_remaining
    end
    
    change_table :savings_accounts do |t|
      t.remove :amount_deposited
      rename_column :savings_accounts, :current_amount_deposited, :current_amount
    end
  end
end
