class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :name
      t.boolean :recurring
      t.decimal :amount
      t.datetime :trans_date
      t.references :expense_category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
