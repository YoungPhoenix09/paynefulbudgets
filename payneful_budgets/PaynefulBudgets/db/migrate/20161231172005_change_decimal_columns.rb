class ChangeDecimalColumns < ActiveRecord::Migration
  def up
    change_column :expenses, :amount, :decimal, precision: 16, scale: 2
    change_column :income_items, :amount, :decimal, precision: 16, scale: 2
    change_column :savings_deposits, :amount, :decimal, precision: 16, scale: 2
    change_column :debt_payments, :amount, :decimal, precision: 16, scale: 2
    change_column :budgets, :net_income, :decimal, precision: 16, scale: 2
    change_column :budgets, :balance, :decimal, precision: 16, scale: 2
    change_column :debts, :amount_remaining, :decimal, precision: 16, scale: 2
    change_column :debts, :current_amount_remaining, :decimal, precision: 16, scale: 2
    change_column :expense_categories, :budgeted_amount, :decimal, precision: 16, scale: 2
    change_column :expense_categories, :remaining_amount, :decimal, precision: 16, scale: 2
    change_column :savings_accounts, :current_amount_deposited, :decimal, precision: 16, scale: 2
    change_column :savings_accounts, :amount_deposited, :decimal, precision: 16, scale: 2
  end
  
  def down
    change_column :expenses, :amount, :decimal, precision: 10, scale: 0
    change_column :income_items, :amount, :decimal, precision: 10, scale: 0
    change_column :savings_deposits, :amount, :decimal, precision: 10, scale: 0
    change_column :debt_payments, :amount, :decimal, precision: 10, scale: 0
    change_column :budgets, :net_income, :decimal, precision: 10, scale: 0
    change_column :budgets, :balance, :decimal, precision: 10, scale: 0
    change_column :debts, :amount_remaining, :decimal, precision: 10, scale: 0
    change_column :debts, :current_amount_remaining, :decimal, precision: 10, scale: 0
    change_column :expense_categories, :budgeted_amount, :decimal, precision: 10, scale: 0
    change_column :expense_categories, :remaining_amount, :decimal, precision: 10, scale: 0
    change_column :savings_accounts, :current_amount_deposited, :decimal, precision: 10, scale: 0
    change_column :savings_accounts, :amount_deposited, :decimal, precision: 10, scale: 0
  end
end
