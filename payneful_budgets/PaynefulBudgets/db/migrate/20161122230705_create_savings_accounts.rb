class CreateSavingsAccounts < ActiveRecord::Migration
  def change
    create_table :savings_accounts do |t|
      t.string :name
      t.string :category
      t.decimal :current_amount
      t.references :budget, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
