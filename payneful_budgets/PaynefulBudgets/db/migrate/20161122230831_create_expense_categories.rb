class CreateExpenseCategories < ActiveRecord::Migration
  def change
    create_table :expense_categories do |t|
      t.string :name
      t.decimal :budgeted_amount
      t.references :budget, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
