class AddColumnBudgetActiveToBudgets < ActiveRecord::Migration
  def change
    change_table :budgets do |t|
      t.boolean :budget_active, default: true
    end
  end
end
