class CreateSavingsDeposits < ActiveRecord::Migration
  def change
    create_table :savings_deposits do |t|
      t.datetime :trans_date
      t.decimal :amount
      t.references :savings_account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
