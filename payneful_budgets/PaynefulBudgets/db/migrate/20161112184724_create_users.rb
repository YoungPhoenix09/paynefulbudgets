class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_name, limit: 30
      t.string :first_name, limit: 75
      t.string :last_name, limit:75
      t.datetime :last_login

      t.timestamps null: false
    end
  end
end
