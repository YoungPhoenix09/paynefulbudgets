class CreateIncomeItems < ActiveRecord::Migration
  def change
    create_table :income_items do |t|
      t.references :budget
      
      t.string :name
      t.string :category
      t.boolean :recurring
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
