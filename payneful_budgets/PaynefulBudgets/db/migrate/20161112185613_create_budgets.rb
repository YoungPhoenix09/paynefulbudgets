class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
      t.decimal :balance
      t.decimal :net_income
      t.string :month
      t.string :year

      t.timestamps null: false
    end
  end
end
