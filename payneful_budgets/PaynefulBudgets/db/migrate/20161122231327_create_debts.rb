class CreateDebts < ActiveRecord::Migration
  def change
    create_table :debts do |t|
      t.string :name
      t.decimal :amount_remaining
      t.references :budget, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
