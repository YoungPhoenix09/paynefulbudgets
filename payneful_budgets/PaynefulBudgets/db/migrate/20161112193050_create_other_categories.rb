class CreateOtherCategories < ActiveRecord::Migration
  def change
    create_table :other_categories do |t|
      t.string :name, limit: 75
      t.string :category_type, limit: 10
      t.references :user

      #t.timestamps null: false
    end
  end
end
