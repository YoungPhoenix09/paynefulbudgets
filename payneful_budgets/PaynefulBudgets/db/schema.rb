# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161231172005) do

  create_table "accounts", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "budget_id",  limit: 4
    t.boolean  "owner"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "budgets", force: :cascade do |t|
    t.decimal  "balance",                   precision: 16, scale: 2
    t.decimal  "net_income",                precision: 16, scale: 2
    t.string   "month",         limit: 255
    t.string   "year",          limit: 255
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.boolean  "budget_active",                                      default: true
  end

  create_table "debt_payments", force: :cascade do |t|
    t.boolean  "recurring"
    t.decimal  "amount",               precision: 16, scale: 2
    t.datetime "trans_date"
    t.integer  "debt_id",    limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "debt_payments", ["debt_id"], name: "index_debt_payments_on_debt_id", using: :btree

  create_table "debts", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.decimal  "amount_remaining",                     precision: 16, scale: 2
    t.integer  "budget_id",                limit: 4
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.decimal  "current_amount_remaining",             precision: 16, scale: 2
  end

  add_index "debts", ["budget_id"], name: "index_debts_on_budget_id", using: :btree

  create_table "expense_categories", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.decimal  "budgeted_amount",              precision: 16, scale: 2
    t.integer  "budget_id",        limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.decimal  "remaining_amount",             precision: 16, scale: 2
  end

  add_index "expense_categories", ["budget_id"], name: "index_expense_categories_on_budget_id", using: :btree

  create_table "expenses", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.boolean  "recurring"
    t.decimal  "amount",                          precision: 16, scale: 2
    t.datetime "trans_date"
    t.integer  "expense_category_id", limit: 4
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
  end

  add_index "expenses", ["expense_category_id"], name: "index_expenses_on_expense_category_id", using: :btree

  create_table "income_items", force: :cascade do |t|
    t.integer  "budget_id",  limit: 4
    t.string   "name",       limit: 255
    t.string   "category",   limit: 255
    t.boolean  "recurring"
    t.decimal  "amount",                 precision: 16, scale: 2
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  create_table "other_categories", force: :cascade do |t|
    t.string  "name",          limit: 75
    t.string  "category_type", limit: 10
    t.integer "user_id",       limit: 4
  end

  create_table "savings_accounts", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.string   "category",                 limit: 255
    t.decimal  "current_amount_deposited",             precision: 16, scale: 2
    t.integer  "budget_id",                limit: 4
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.decimal  "amount_deposited",                     precision: 16, scale: 2
  end

  add_index "savings_accounts", ["budget_id"], name: "index_savings_accounts_on_budget_id", using: :btree

  create_table "savings_deposits", force: :cascade do |t|
    t.datetime "trans_date"
    t.decimal  "amount",                       precision: 16, scale: 2
    t.integer  "savings_account_id", limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
  end

  add_index "savings_deposits", ["savings_account_id"], name: "index_savings_deposits_on_savings_account_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "user_name",       limit: 30
    t.string   "first_name",      limit: 75
    t.string   "last_name",       limit: 75
    t.datetime "last_login"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "password_digest", limit: 255
  end

  add_foreign_key "debt_payments", "debts"
  add_foreign_key "debts", "budgets"
  add_foreign_key "expense_categories", "budgets"
  add_foreign_key "expenses", "expense_categories"
  add_foreign_key "savings_accounts", "budgets"
  add_foreign_key "savings_deposits", "savings_accounts"
end
